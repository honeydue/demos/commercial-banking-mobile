package cloud.honeydue.api.controller;

import cloud.honeydue.api.model.User;
import cloud.honeydue.api.service.UserService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "User Controller", description = "User API")
@RequestMapping(value="/api/v1", produces="application/json")
@CrossOrigin(origins="*")
public class UserController {
    Logger logger = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public List<User> findAll() {
        return userService.findAll();
    }

    @GetMapping("/users/{id}")
    public Optional<User> findUserById(@PathVariable("id") Long id) {
        return userService.findById(id);
    }

//    @GetMapping("/users/sayHello")
//    public String sayHello(@RequestParam(value = "name", defaultValue = "world") String name) {
//        return "{ \"response\": \"hello " + name + "\" }";
//    }
}
