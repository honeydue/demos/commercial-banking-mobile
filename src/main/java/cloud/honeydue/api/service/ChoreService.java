package cloud.honeydue.api.service;

import cloud.honeydue.api.model.Chore;
import cloud.honeydue.api.repository.ChoreRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChoreService {
    private final ChoreRepository choreRepository;

    public ChoreService(ChoreRepository choreRepository) {
        this.choreRepository = choreRepository;
    }

    public List<Chore> findByUserId(Long id) {
        return choreRepository.findByUserId(id);
    }
}
